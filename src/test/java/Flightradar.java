import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.TimerTask;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class Flightradar {

    private static final double MY_LATITUDE = 52.199543080778;
    private static final double MY_LONGITUDE = 21.445520360656104;

    public static void main(String[] args) throws IOException {

        String json = getResultAmount("test");

        JSONParser parser = new JSONParser();
        JSONObject jsonObject = null;

        try {
            jsonObject = (JSONObject)parser.parse(json);
        } catch (org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }

        for (Object key : jsonObject.keySet()) {
            Object value = jsonObject.get(key);
            // TODO tutaj obsłuż obiekt "value", możesz go podejrzeć w debuggerze
        }

        showInformations();

        new java.util.Timer().schedule(new TimerTask(){
            @Override
            public void run() {
                try {
                    showInformations();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        },30*1000,30*1000);
    }

    private static void showInformations() throws IOException {
        String json = getResultAmount("test");
        int planeCount = 0;
        String[] str = new String[json.length()];

        for (int i = 0; i < json.length(); i ++) {
            str[i] = json.substring(i, i + 1);
        }

        for (String s : str) {
            if (s.equals("[")) {
                planeCount++;
            }
        }

        String[] latitude = new String[planeCount];
        String[] longtitude = new String[planeCount];
        String[] planeName = new String[planeCount];

        for (int i = 0; i < planeCount; i++) {
            latitude[i] = "";
            longtitude[i] = "";
            planeName[i] = "";
        }

        planeCount = 0;
        int j = 0;
        int latitudePos = 1;
        int longitudePos = 2;
        int planeNamePos = 16;

        for (int i = 30; i < str.length; i++) {
            if (str[i].equals("[")) {
                planeCount++;
                j = 0;
            }
            if (str[i].equals(",") & planeCount > 0) {
                j++;
            } else if (j == latitudePos) {
                latitude[planeCount - 1] += str[i];
            } else if (j == longitudePos) {
                longtitude[planeCount - 1] += str[i];
            } else if (j == planeNamePos) {
                planeName[planeCount - 1] += str[i];
            }
        }



        double[] latitudeNumber = new double[planeCount];
        double[] longitudeNumber = new double[planeCount];
        int[] distance = new int[planeCount];

        for (int i = 0; i < planeCount; i++) {
            latitudeNumber[i] = Double.parseDouble(latitude[i]);
            longitudeNumber[i] = Double.parseDouble(longtitude[i]);
            distance[i] = (int) distance(latitudeNumber[i], MY_LATITUDE, longitudeNumber[i], MY_LONGITUDE);
            planeName[i] = planeName[i].substring(1, planeName[i].length() - 1);
        }

        System.out.println("Liczba samolotów: " + planeCount);

        for (int i = 0; i < planeCount; i++) {
            System.out.println(planeName[i] + " | " + distance[i] + "km");
        }
    }

    private static String getResultAmount(String query) throws IOException {
        String url = "https://data-live.flightradar24.com/zones/fcgi/feed.js?faa=1&bounds=52.448%2C51.938%2C17.561%2C20.93&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&selected=2903a51c&ems=1&stats=1&fbclid=IwAR1KGuPTCgEROBXdL8oqfsVPGTeuQHYFb4q6uIhaBOmFgLAD4_TvtPkfaj4";
        URLConnection connection = new URL(url + query).openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        connection.connect();

        BufferedReader r  = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            sb.append(line);
        }

        return sb.toString();

    }

    private static double distance(double lat1,
                                   double lat2, double lon1,
                                   double lon2)
    {

        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2),2);

        double c = 2 * Math.asin(Math.sqrt(a));

        double r = 6371;
        return c * r;
    }
}