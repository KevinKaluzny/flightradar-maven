import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.TimerTask;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class FlightradarWithMaven {
    private static final double MY_LATITUDE = 52.199543080778;
    private static final double MY_LONGITUDE = 21.445520360656104;

    public static void main(String[] args) throws IOException {
        showInformations();

        new java.util.Timer().schedule(new TimerTask(){
            @Override
            public void run() {
                try {
                    showInformations();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        },30*1000,30*1000);
    }

    private static void showInformations() throws IOException {
        String json = getResultAmount("test");
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = null;
        Object value;
        int planeCount = 0;

        try {
            jsonObject = (JSONObject)parser.parse(json);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (Object key : jsonObject.keySet()) {
            value = jsonObject.get(key);
            try {
                if (((JSONArray) value).size() > 1) {
                    planeCount++;
                }
            } catch (Exception e) {}
            // TODO tutaj obsłuż obiekt "value", możesz go podejrzeć w debuggerze
        }

        String[] latitude = new String[planeCount];
        String[] longitude = new String[planeCount];
        String[] planeName = new String[planeCount];
        double[] latitudeNumber = new double[planeCount];
        double[] longitudeNumber = new double[planeCount];
        int[] distance = new int[planeCount];
        int latitudePos = 1;
        int longitudePos = 2;
        int planeNamePos = 16;
        planeCount = 0;

        for (Object key : jsonObject.keySet()) {
            value = jsonObject.get(key);
            try {
                if (((JSONArray) value).size() > 1) {
                    latitude[planeCount] = ((JSONArray) value).get(latitudePos).toString();
                    longitude[planeCount] = ((JSONArray) value).get(longitudePos).toString();
                    planeName[planeCount] = ((JSONArray) value).get(planeNamePos).toString();
                    latitudeNumber[planeCount] = Double.parseDouble(latitude[planeCount]);
                    longitudeNumber[planeCount] = Double.parseDouble(longitude[planeCount]);
                    distance[planeCount] = (int) distance(latitudeNumber[planeCount], MY_LATITUDE, longitudeNumber[planeCount], MY_LONGITUDE);
                    planeCount++;
                }
            } catch (Exception e) {}
            // TODO tutaj obsłuż obiekt "value", możesz go podejrzeć w debuggerze
        }

        System.out.println("Liczba samolotów: " + planeCount);

        for (int i = 0; i < planeCount; i++) {
            System.out.println(planeName[i] + " | " + distance[i] + "km");
        }
    }

    private static String getResultAmount(String query) throws IOException {
        String url = "https://data-cloud.flightradar24.com/zones/fcgi/feed.js?faa=1&bounds=62.593%2C-4.304%2C-48.78%2C47.02&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&selected=2c0a3a2a&ems=1&stats=1";
        URLConnection connection = new URL(url + query).openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        connection.connect();

        BufferedReader r  = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            sb.append(line);
        }

        return sb.toString();
    }

    private static double distance(double lat1,
                                   double lat2, double lon1,
                                   double lon2)
    {
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2),2);

        double c = 2 * Math.asin(Math.sqrt(a));

        double r = 6371;
        return c * r;
    }
}